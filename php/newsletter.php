<?php 
define("WEBMASTER_EMAIL", 'rod.selt@gmail.com');
header('Content-type: application/json; charset=utf-8');
if(isset($_POST['email'])) {
  $contacto = false;
  $nombre = "NewsLetter";
  $email = $_POST['email'];
  $asunto_email = "Suscripción al NewsLetter";
  $email_message = "Detalles del formulario:\n\n";
  $email_message .= "E-mail: " . $email . "\n";
  // Ahora se envía el e-mail usando la función mail() de PHP
  $headers = "From: ".$nombre." <".$email.">\r\n"."Reply-To: ".$email."\r\n"."X-Mailer: PHP/" . phpversion();
  $contacto = mail(WEBMASTER_EMAIL, $asunto_email, $email_message, $headers);
  if ($contacto) {
    echo '{"state":"ok","info":"¡Gracias por suscribirse!"}';
  } else{
    echo '{"state":"error","info":"Error al suscribirse. Intente nuevamente."}';
  }
} else{
  echo '{"state":"error","info":"Complete los datos Faltantes"}';
}
?>